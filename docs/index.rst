.. include:: ../README.rst

.. _table-of-contents:

Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: Salt basics

   topics/overview
   topics/installation
   topics/grains
   topics/state-system
   topics/states
   topics/jinja
   topics/pillar
   topics/map-files

.. toctree::
   :maxdepth: 2
   :caption: Intermediate Salt

   topics/beacons
   topics/reactors
   topics/runners-orchestration
   topics/requisites
   topics/execution-framework
   topics/salt-ssh
   topics/troubleshooting
   topics/scheduler
   topics/execution-architecture
   topics/security

.. toctree::
   :maxdepth: 2
   :caption: Contributing

   topics/contributing
   topics/writing-salt-docs
   topics/style-guide
   topics/list-of-images
